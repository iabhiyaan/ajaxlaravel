<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/about','admin\AboutUsController@index');

// Form get
// Route::get('/form','admin\AboutUsController@show');

// Form post
// Route::post('/postformdata','admin\AboutUsController@postformdata');
// deleteitem
// Route::get('/deleteitem/{id}','admin\AboutUsController@delete');



//for update

//to get the data
// Route::get('/aboutedit/{id}','admin\AboutUsController@update');
// Route::post('/editformdata/{id}','admin\AboutUsController@updateformdata');

Route::get('/','admin\HomeController@index');
Route::resource('about', 'admin\AboutController');
Route::resource('product','admin\ProductController');
Route::resource('order','admin\OrderController');
Route::post('productSave','admin\ProductController@save');
Route::post('updateSave','admin\ProductController@aupdate');
Route::post('delete','admin\ProductController@delete');
