<!DOCTYPE html>
<html lang="en">

@include('includes.header')

<body class="bg-gradient-primary">

    @yield('content')

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    @include('includes.scripts')

</body>

</html>