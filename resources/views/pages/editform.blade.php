@extends('welcome')
@section('content')
<div class="container my-4">
    <div class="row">
        <div class="col-12">
            <h1>Edit Form</h1>
        </div>
        <!-- Update -->
        <div class="col-7">

            <form action="{{route('about.update',$rec->id)}}" method="POST" role="form" enctype="multipart/form-data" >
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="heading">Heading:</label>
                    <input name="heading" type="text" class="form-control" placeholder="Enter Heading" id="heading"
                        value="{{$rec->heading}}">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" type="text" rows="5" class="form-control"
                        placeholder="Enter Description" id="description">{{$rec->description}}</textarea>
                </div>
                <label>Status:</label>
                <div class="form-group">
                    <label for="date">Updated date:</label>
                    <input name="date" type="date" class="form-control" placeholder="Enter Date" id="date"
                        value="{{$rec->updated_date}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection