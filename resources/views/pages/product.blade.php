@extends('welcome')
@section('content')
<!-- Begin Page Content -->

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product Listing Table</h1>
    <p class="mb-4">These all are products available.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Products</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info mb-3" id="showaddModal" data-toggle="modal"
                    data-target="#addProducts">
                    Add Products
                </button>
                <div class="modal fade" id="addProducts" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalTitle"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            @csrf
                            <div class="modal-body">
                                <input type="hidden" id="modalId" />
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="Enter product">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" name="description" class="form-control" id="description"
                                        placeholder="Enter Description">
                                </div>
                                <div class="form-group">
                                    <label for="updated_date">Updated Date</label>
                                    <input type="date" name="updated_date" class="form-control" id="updated_date"
                                        placeholder="Enter Date">
                                </div>
                                <button type="submit" id="addProductButton" data-dismiss="modal"
                                    class="btn btn-info mb-3">Add Product</button>
                                <button type="submit" data-dismiss="modal" id="editProductButton" class="btn btn-info mb-3">Edit
                                    Product</button>
                                <button type="submit" id="deleteProductButton" class="btn btn-info mb-3">Delete
                                    Product</button>
                                <button type="submit" data-dismiss="modal" id="saveProductButton" class="btn btn-info mb-3">Save
                                    Product</button>
                                <button class="btn btn-danger mb-3" data-dismiss="modal">Cancel
                                    Product!!</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Updated Date</th>
                            <th>Function</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        @foreach ($products as $p)
                        <tr id="tr">
                            <input id="getId" type="hidden" value={{ $p->id }} />
                            <td> {{ $p->id }} </td>
                            <td> {{ $p->name }} </td>
                            <td> {{ $p->description }} </td>
                            <td> {{ $p->updated_date }} </td>
                            <td>
                                <a id="editProductButton" data-toggle="modal" data-target="#addProducts"
                                    class="btn btn-info mb-3 text-white btn-block">Edit</a>
                                <button id="deleteProductButton" class="btn btn-danger btn-block">Delete</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<!-- /.container-fluid -->
@endsection



{{-- <div class="modal fade" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="editProductTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('product.update', $editproduct->id)}}" id="form"
enctype="multipart/form-data">
@csrf
@method('PUT')
<div class="form-group">
    <label for="name">Name</label>
    <input value="{{$product->name}}" type="text" name="name" class="form-control" id="name"
        placeholder="Enter product">
</div>
<div class="form-group">
    <label for="description">Description</label>
    <input value="{{$editproduct->description}}" type="text" name="description" class="form-control" id="description"
        placeholder="Enter Description">
</div>
<div class="form-group">
    <label for="updated_date">Updated Date</label>
    <input type="date" name="updated_date" class="form-control" id="updated_date" placeholder="Enter updated_date">
</div>
<button type="submit" class="btn btn-info mb-3">Edit Product!!</button>
<button type="submit" class="btn btn-danger mb-3" data-dismiss="modal">Cancel Product!!</button>
</form>

</div>
</div>
</div>
</div> --}}