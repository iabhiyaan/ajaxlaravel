<script src="{{asset('admin/js/jquery.min.js')}}"></script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->
<script src=" {{ asset('admin/js/popper.min.js') }} "></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('admin/js/sb-admin-2.min.js')}}"></script>

<!-- Page level plugins -->
<script src="{{asset('admin/vendor/chart.js/Chart.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('admin/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('admin/js/demo/chart-pie-demo.js')}}"></script>
<script src=" {{asset('admin/js/custom.js')}} "></script>