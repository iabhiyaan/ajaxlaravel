$(document).ready(function() {
	// ========= Show Modal ========= //
	$(document).on("click", "#showaddModal", function() {
		$("#modalTitle").text("Add Products!!");
		$("#addProductButton").show();
		$("#editProductButton").hide();
		$("#deleteProductButton").hide();
		$("#saveProductButton").hide();
		$("#name").val("");
		$("#description").val("");
		$("#updated_date").val("");
	});
	// ========= Store Modal ========= //
	$(document).on("click", "#addProductButton", function() {
		var grabname = $("#name").val();
		var grabdescription = $("#description").val();
		var grabupdated_date = $("#updated_date").val();
		console.log(grabname);
		console.log(grabdescription);
		console.log(grabupdated_date);

		// Grab all the text from input
		$.post(
			"productSave",
			{
				name: grabname,
				description: grabdescription,
				updated_date: grabupdated_date,
				_token: $("input[name='_token']").val()
			},
			function(data) {
				$("#name").val("");
				$("#description").val("");
				$("#updated_date").val("");
				$("#tbody").load(location.href + " #tr");
			}
		);
	});

	// ========= Edit Modal ========= //
	$(document).on("click", "#editProductButton", function() {
		$("#modalTitle").text("Edit Product!!");
		$("#addProductButton").hide();
		$("#editProductButton").hide();
		$("#deleteProductButton").hide();
		$("#saveProductButton").show();
		var $tr = $(this).closest("tr");
		var getData = $tr
			.children("td")
			.map(function() {
				return $(this).text();
			})
			.get();
		$("#name").val(getData[1]);
		$("#description").val(getData[2]);
		$("#updated_date").val(getData[3]);
		console.log(getData);
	});
	// ========= Update Modal ========= //

	$(document).on("click", "#saveProductButton", function() {
		var grabId = $("#getId").val();
		console.log(grabId);
		var grabname = $("#name").val();
		var grabdescription = $("#description").val();
		var grabupdateddate = $("#updated_date").val();
		$.post(
			"updateSave",
			{
				id: grabId,
				name: grabname,
				description: grabdescription,
				updated_date: grabupdateddate,
				_token: $('input[name="_token"]').val()
			},
			function(data) {
				console.log(data);
				$("#name").val("");
				$("#description").val("");
				$("#updated_date").val("");
				$("#tbody").load(location.href + " #tr");
			}
		);
	});
	// ========= Delete Modal ========= //
	$(document).on("click", "#deleteProductButton", function() {
		var tr = $(this)
			.closest("tr")
			.children("td")
			.map(function() {
				return $(this).text();
			})
			.get();
		var getId = tr[0];
		$.post(
			"delete",
			{
				id: getId,
				_token: $('input[name="_token"]').val()
			},
			function(data) {
				console.log(data);
				$("#tbody").load(location.href + " #tr");
			}
		);
		console.log(getId);
	});
});
