<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\About;
// use DB;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
    {
        // $records = DB::table('tbl_about')->get();
        $records = About::orderBy('updated_date', 'desc')->get();
        // dd($records);
        return view('pages.about')->with('records', $records);
        // return view('pages.about',compact('$records'));
    }
    public function show()
    {
        return view('pages.form');
    }
    public function postformdata(Request $request)
    {
        // dd($request->all());
        $about = new About();
        $about->heading = $request->heading;
        $about->description = $request->description;
        $about->status = $request->status;
        $about->updated_date = $request->date;
        $about->save();
        return Redirect('/about')->withMesage('Success');
    }
    public function update(Request $request, $id)
    {
        $rec = About::find($id);
        return view('pages.editform', ['rec' => $rec]);
    }

    public function updateformdata(Request $request, $id)
    {
        $rec = About::find($id);
        $rec->heading = $request->heading;
        $rec->description = $request->description;
        $rec->updated_date = $request->date;
        $rec->save();
        return Redirect('/about')->withMesage('Success');
    }
    // delete
    public function delete($id)
   {
       $rec = About::find($id);
       $rec -> delete();
       return Redirect('/about');
   }
}
