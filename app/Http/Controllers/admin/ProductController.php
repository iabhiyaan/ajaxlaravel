<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('pages.product')->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->updated_date = $request->updated_date;
        $product->save();
        return $request->all();
    }
    
    public function save(Request $request) {
        $product = new Product;
        $product->name= $request->name;
        $product->description = $request->description;
        $product->updated_date = $request->updated_date;
        $product->save();
        return $request->all();
    }
    public function aupdate(Request $request)
    {
        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->updated_date = $request->updated_date;
        $product->save();
        return $request->all();
    }
    public function delete(Request $request)
    {
        $product = Product::where('id',$request->id)->delete();
        return $request->all();
    }
}